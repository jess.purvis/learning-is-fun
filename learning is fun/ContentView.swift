//
//  ContentView.swift
//  learning is fun
//
//  Created by Jess Purvis on 17/10/2022.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
            Image(systemName: "globe")
            
            HStack {
                Text("Hello, worlf!")
                    .font(.title)
                    .foregroundColor(Color.red)
                Image(systemName: "pencil.circle.fill")
                    .imageScale(.large)
                    .foregroundColor(.accentColor)
                
            }
        }
        .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
