//
//  learning_is_funApp.swift
//  learning is fun
//
//  Created by Jess Purvis on 17/10/2022.
//

import SwiftUI

@main
struct learning_is_funApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
